# 仓库初始化

#### 1、删除原有云主机，重新购买

| 主机名称 | IP地址        | 最低配置    |
| -------- | ------------- | ----------- |
| registry | 192.168.1.100 | 1CPU,1G内存 |

#### 2、安装仓库服务

```shell
[root@registry ~]# yum makecache
[root@registry ~]# yum install -y docker-distribution
[root@registry ~]# systemctl enable --now docker-distribution
```

#### 3、使用脚本初始化仓库

**拷贝云盘 kubernetes/v1.17.6/registry/myos目录 到 仓库服务器**

```shell
[root@registry ~]# cd myos
[root@registry ~]# chmod 755 init-img.sh
[root@registry ~]# ./init-img.sh
[root@registry ~]# curl http://192.168.1.100:5000/v2/myos/tags/list
{"name":"myos","tags":["nginx","php-fpm","v1804","httpd"]}
```

