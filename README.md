# Tedu_NSD
云计算第四阶段课程笔记

## 讲师介绍
李欣
20年一线工作经验，曾在魅力企业网，英才网联、建筑英才网、人民网旗下澳客网担任系统架构师、运维经理等职务，精通 Linux 下服务的配置，大规模集群管理，针对网络架构、系统优化，性能调优有独到见解，擅长使用 shell、python 开发等。

授课风格
思路清晰，考点把握精准，语言幽默风趣

讲师QQ: 3273252716
仓库地址 https://gitee.com/luckfurit/Tedu_NSD.git

## 本阶段所有软件下载地址
https://pan.baidu.com/s/1xsKK5bwVcGUnTuxbStjIGA 
提取码:nprh

typora 编辑器下载地址
https://typora.io